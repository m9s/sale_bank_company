#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Equal, Eval
from trytond.transaction import Transaction
from trytond.pool import Pool


class Sale(ModelSQL, ModelView):
    _name = 'sale.sale'

    bank_account = fields.Many2One('bank.account', 'Bank Account', domain=[
                ('company_account','=','True'),
            ], context={'company': Eval('company')}, required=True, states={
                'readonly': Not(Equal(Eval('state'), 'draft')),
            }, depends=['company', 'state'])

    def default_bank_account(self):
        company_obj = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context.get('company'))
            if company.bank_accounts:
                return company.bank_accounts[0].id
        return {}

    def create_invoice(self, sale_id):
        '''
        Create an invoice for the sale

        :param sale_id: the sale id

        :return: the created invoice id or None
        '''
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        journal_obj = pool.get('account.journal')
        invoice_line_obj = pool.get('account.invoice.line')
        sale_line_obj = pool.get('sale.line')

        context = {}
        sale = self.browse(sale_id)

        invoice_lines = self._get_invoice_line_sale_line(sale)
        if not invoice_lines:
            return

        journal_id = journal_obj.search([
            ('type', '=', 'revenue'),
            ], limit=1)
        if journal_id:
            journal_id = journal_id[0]

        context['user'] = user
        with Transaction().set_user(0):
            with Transaction().set_context(**context):
                invoice_id = invoice_obj.create({
                    'company': sale.company.id,
                    'type': 'out_invoice',
                    'reference': sale.reference,
                    'journal': journal_id,
                    'party': sale.party.id,
                    'invoice_address': sale.invoice_address.id,
                    'currency': sale.currency.id,
                    'account': sale.party.account_receivable.id,
                    'payment_term': sale.payment_term.id,
                    'bank_account': sale.bank_account.id,
                })

        for line_id in invoice_lines:
            for vals in invoice_lines[line_id]:
                vals['invoice'] = invoice_id
                with Transaction().set_user(0):
                    with Transaction().set_context(**context):
                        invoice_line_id = invoice_line_obj.create(vals)
                sale_line_obj.write(line_id, {
                    'invoice_lines': [('add', invoice_line_id)],
                    })

        with Transaction().set_user(0):
            with Transaction().set_context(**context):
                invoice_obj.update_taxes([invoice_id])

        self.write(sale_id, {
            'invoices': [('add', invoice_id)],
        })
        return invoice_id

Sale()
