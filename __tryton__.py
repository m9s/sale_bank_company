#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Sale Bank Company',
    'name_de_DE': 'Verkauf Bankverbindung Unternehmen',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Sale Bank Company
    - Adds a field bank account to sales
      to be shown on reports.
''',
    'description_de_DE': '''Verkauf Bankverbindung Unternehmen
    - Fügt das Feld Bankkonto zu Verkäufen hinzu
    - Dient der Angabe der Bankverbindung des Unternehmens
      auf den entsprechenden Berichten.
''',
    'depends': [
        'account_invoice_bank_company',
        'party_bank',
        'sale',
    ],
    'xml': [
        'sale.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
